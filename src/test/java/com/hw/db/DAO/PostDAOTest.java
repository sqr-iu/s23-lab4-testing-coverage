package com.hw.db.DAO;

import com.hw.db.models.Post;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;
import java.util.Optional;
import java.util.stream.Stream;

public class PostDAOTest {
    private JdbcTemplate mockJdbc;
    private Post post1;
    static String author1 = "John";
    static String message1 = "Hello!";
    static Timestamp timestamp1 = new Timestamp(1234567);
    static String author2 = "Kristian";
    static String message2 = "Hi!";
    static Timestamp timestamp2 = new Timestamp(7654321);
    @BeforeEach
    void setUp() {
        post1 = new Post();
        post1.setAuthor(author1);
        post1.setMessage(message1);
        post1.setCreated(timestamp1);
        mockJdbc = Mockito.mock(JdbcTemplate.class);
        new PostDAO(mockJdbc);
        Mockito.when(mockJdbc.queryForObject(Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"), Mockito.any(PostDAO.PostMapper.class), Mockito.eq(0))).thenReturn(post1);
    }
    private static Stream<Arguments> params() {
        return Stream.of(Arguments.of(0, author1, message1, timestamp2, "UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Arguments.of(0, author1, message2, timestamp1, "UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"), Arguments.of(0, author1, message2, timestamp2, "UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Arguments.of(0, author2, message1, timestamp1, "UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"), Arguments.of(0, author2, message1, timestamp2, "UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"), Arguments.of(0, author2, message2, timestamp1, "UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"), Arguments.of(0, author2, message2, timestamp2, "UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"));
    }

    @ParameterizedTest
    @MethodSource("params")
    void TestSetPost(Integer id, String author, String message, Timestamp ts, String q) {
        Post post = new Post();
        post.setAuthor(author);
        post.setMessage(message);
        post.setCreated(ts);
        PostDAO.setPost(id, post);
        Mockito.verify(mockJdbc).update(Mockito.eq(q), Optional.ofNullable(Mockito.any()));
    }

    @Test
    void TestNoSetPost() {
        PostDAO.setPost(0, post1);
        Mockito.verify(mockJdbc, Mockito.never()).update(Mockito.anyString(), Optional.ofNullable(Mockito.any()));
    }
}
